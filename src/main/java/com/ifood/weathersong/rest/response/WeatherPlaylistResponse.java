package com.ifood.weathersong.rest.response;

import java.io.Serializable;

import com.ifood.weathersong.domain.Playlist;

public class WeatherPlaylistResponse implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Playlist playlist;

	public Playlist getPlaylist() {
		return playlist;
	}

	public void setPlaylist(Playlist playlist) {
		this.playlist = playlist;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((playlist == null) ? 0 : playlist.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		WeatherPlaylistResponse other = (WeatherPlaylistResponse) obj;
		if (playlist == null) {
			if (other.playlist != null)
				return false;
		} else if (!playlist.equals(other.playlist))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "WeatherPlaylistResponse [playlist=" + playlist + "]";
	}
	
}
