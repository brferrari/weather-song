package com.ifood.weathersong.spotify.client.response;

public class SpotifyTrackItemResponse {

	private SpotifyTrackItemTrackResponse track;

	public SpotifyTrackItemTrackResponse getTrack() {
		return track;
	}

	public void setTrack(SpotifyTrackItemTrackResponse track) {
		this.track = track;
	}
	
}
