package com.ifood.weathersong;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

@ComponentScan
@EnableWebSecurity
@EnableAutoConfiguration
@SpringBootApplication
public class WeatherSongApplication {

	public static void main(String[] args) {
		SpringApplication.run(WeatherSongApplication.class, args);
	}
	
}
