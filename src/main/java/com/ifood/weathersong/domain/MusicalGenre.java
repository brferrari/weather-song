package com.ifood.weathersong.domain;

import java.util.stream.Stream;

public enum MusicalGenre {

	PARTY("party", 31, 200), POP("pop", 15, 30), ROCK("rock", 10, 14), CLASSICAL("classical", 0, 9);
	
	private String category;
	
	private int minCelsius;
	
	private int maxCelsius;

	private MusicalGenre(String category, int minCelsius, int maxCelsius) {
		this.category = category;
		this.minCelsius = minCelsius;
		this.maxCelsius = maxCelsius;
	}

	public String getCategory() {
		return category;
	}
	
	public int getMinCelsius() {
		return minCelsius;
	}

	public int getMaxCelsius() {
		return maxCelsius;
	}

	public static MusicalGenre getGenreByTemperature(final Double temperature) {
		return Stream.of(MusicalGenre.values()).filter(music -> temperature >= music.getMinCelsius() && temperature <= music.getMaxCelsius())
				.findFirst().orElse(null);
	}
	
}
