package com.ifood.weathersong.spotify.client.response;

import java.io.Serializable;

public class SpotifyPlaylistItemResponse implements Serializable {

	private static final long serialVersionUID = 1L;

	private String id;
	
	private String name;
	
	private SpotifyPlaylistItemTrackResponse tracks;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public SpotifyPlaylistItemTrackResponse getTracks() {
		return tracks;
	}

	public void setTracks(SpotifyPlaylistItemTrackResponse tracks) {
		this.tracks = tracks;
	}

}
