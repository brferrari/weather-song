package com.ifood.weathersong.domain;

import java.io.Serializable;

public class Track implements Serializable {

	private static final long serialVersionUID = 1L;

	private String trackName;

	public Track(String trackName) {
		this.trackName = trackName;
	}

	public String getTrackName() {
		return trackName;
	}

	public void setTrackName(String trackName) {
		this.trackName = trackName;
	}
	
}
