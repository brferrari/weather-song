package com.ifood.weathersong.spotify.client.response;

import java.io.Serializable;
import java.util.List;

public class SpotifyPlaylistResponse implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<SpotifyPlaylistItemResponse> items;

	public List<SpotifyPlaylistItemResponse> getItems() {
		return items;
	}

	public void setItems(List<SpotifyPlaylistItemResponse> items) {
		this.items = items;
	}
	
}
