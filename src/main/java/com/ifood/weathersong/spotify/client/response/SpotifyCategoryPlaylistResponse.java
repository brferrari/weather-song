package com.ifood.weathersong.spotify.client.response;

import java.io.Serializable;

public class SpotifyCategoryPlaylistResponse implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private SpotifyPlaylistResponse playlists;

	public SpotifyPlaylistResponse getPlaylists() {
		return playlists;
	}

	public void setPlaylists(SpotifyPlaylistResponse playlists) {
		this.playlists = playlists;
	}
	
}
