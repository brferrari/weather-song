package com.ifood.weathersong.openweather.client.response;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MainCurrentWeatherResponse implements Serializable {

	private static final long serialVersionUID = 1L;

	private Double temp;
	
	private Long pressure;
	
	private Integer humidity;
	
	@JsonProperty("temp_min")
	private Double minTemp;
	
	@JsonProperty("temp_max")
	private Double maxTemp;

	public Double getTemp() {
		return temp;
	}

	public void setTemp(Double temp) {
		this.temp = temp;
	}

	public Long getPressure() {
		return pressure;
	}

	public void setPressure(Long pressure) {
		this.pressure = pressure;
	}

	public Integer getHumidity() {
		return humidity;
	}

	public void setHumidity(Integer humidity) {
		this.humidity = humidity;
	}

	public Double getMinTemp() {
		return minTemp;
	}

	public void setMinTemp(Double minTemp) {
		this.minTemp = minTemp;
	}

	public Double getMaxTemp() {
		return maxTemp;
	}

	public void setMaxTemp(Double maxTemp) {
		this.maxTemp = maxTemp;
	}
	
}
