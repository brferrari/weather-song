package com.ifood.weathersong.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ifood.weathersong.domain.Playlist;
import com.ifood.weathersong.rest.response.WeatherPlaylistResponse;
import com.ifood.weathersong.service.WeatherPlaylistService;

@RestController
public class WeatherPlaylistController {

	@Autowired
	private WeatherPlaylistService weatherPlaylistService;
	
	@RequestMapping(value = "/v1/weatherplaylists", method = RequestMethod.GET)
	public ResponseEntity<WeatherPlaylistResponse> getPlaylist(@RequestParam(name = "latitude") final Double latitude, @RequestParam(name = "longitude") final Double longitude) {
		
		final Playlist playlist = weatherPlaylistService.getPlaylist(latitude, longitude);
		
		WeatherPlaylistResponse response = new WeatherPlaylistResponse();
		response.setPlaylist(playlist);
		
		return new ResponseEntity<WeatherPlaylistResponse>(response, HttpStatus.OK);
		
	}
	
}
