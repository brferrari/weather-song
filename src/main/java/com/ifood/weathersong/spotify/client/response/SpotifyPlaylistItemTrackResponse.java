package com.ifood.weathersong.spotify.client.response;

import java.io.Serializable;

public class SpotifyPlaylistItemTrackResponse implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String href;

	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}
	
}
