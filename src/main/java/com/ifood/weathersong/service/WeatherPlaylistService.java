package com.ifood.weathersong.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ifood.weathersong.domain.MusicalGenre;
import com.ifood.weathersong.domain.Playlist;
import com.ifood.weathersong.openweather.client.OpenWeatherClientService;
import com.ifood.weathersong.spotify.client.SpotifyClientService;

@Service
public class WeatherPlaylistService {

	@Autowired
	private SpotifyClientService spotifyClientService;
	
	@Autowired
	private OpenWeatherClientService openWeatherClientService;
	
	public Playlist getPlaylist(final Double latitude, final Double longitude) {
		
		final Double temperature = openWeatherClientService.getCurrentTemperature(latitude, longitude);
		
		final MusicalGenre musicalGenre = MusicalGenre.getGenreByTemperature(temperature);
		
		if (musicalGenre != null) {
			throw new RuntimeException(String.format("Não foi possível definir um gênero para a temperatura informada [%2.2f].", musicalGenre));
		}
		
		return spotifyClientService.getPlaylist(musicalGenre);
		
	}

}
