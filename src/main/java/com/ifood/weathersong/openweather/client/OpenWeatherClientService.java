package com.ifood.weathersong.openweather.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.ifood.weathersong.openweather.client.response.OpenWeatherCurrentWeatherResponse;

@Service
public class OpenWeatherClientService {
	
	private static final Double KELVIN_SCALE = 273.15;
	private static final String URL = "api.openweathermap.org/data/2.5/weather?lat=%s&lon=%s&APPID=%s";
	
	@Value("${openweather.accesskey}")
	private String appKey;
	
	@Autowired
	private RestTemplate restTemplate;
	
	@Retryable(value = {RuntimeException.class}, maxAttempts = 3, backoff = @Backoff(delay = 300))
	public Double getCurrentTemperature(final Double latitude, final Double longitude) {
		
		final String finalURL = String.format(URL, latitude, longitude, this.appKey);
		
		try {
			final OpenWeatherCurrentWeatherResponse response = restTemplate.getForObject(finalURL, OpenWeatherCurrentWeatherResponse.class);
			
			if (response == null) {
				throw new RuntimeException("Não foi possível obter a temperatura da localização informada.");
			}
			
			return convertKelvinToCelsius(response.getMain().getTemp());
			
		} catch (Exception e) {
			throw new RuntimeException("Não foi possível obter a temperatura da localização informada.");
		}
		
	}
	
	private Double convertKelvinToCelsius(final Double kelvinTemperature) {
		return kelvinTemperature != null ? kelvinTemperature - KELVIN_SCALE : 0;
	}

}
