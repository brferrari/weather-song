package com.ifood.weathersong.spotify.client.response;

import java.io.Serializable;
import java.util.List;

public class SpotifyTrackResponse implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private List<SpotifyTrackItemResponse> items;

	public List<SpotifyTrackItemResponse> getItems() {
		return items;
	}

	public void setItems(List<SpotifyTrackItemResponse> items) {
		this.items = items;
	}

}
