package com.ifood.weathersong.spotify.client.response;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class OAuthResponse implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@JsonProperty("access_token")
	private String accessToken;

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

}
