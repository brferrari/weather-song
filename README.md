**iFood Advanced Backend Test - Projeto WeatherSong**

A solução proposta para o desafio foi um serviço SpringBoot que consiste em uma API REST que recebe as coordenadas de um local
e retorna uma playlist com faixas sugeridas mediante a temperatura atual do local.
Foram utilizadas integrações com os serviços *OpenWeather* e *Spotify* conforme sugerido.

---