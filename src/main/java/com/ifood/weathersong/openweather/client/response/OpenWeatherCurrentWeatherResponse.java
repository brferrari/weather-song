package com.ifood.weathersong.openweather.client.response;

import java.io.Serializable;

public class OpenWeatherCurrentWeatherResponse implements Serializable {

	private static final long serialVersionUID = 1L;

	private MainCurrentWeatherResponse main;

	public MainCurrentWeatherResponse getMain() {
		return main;
	}

	public void setMain(MainCurrentWeatherResponse main) {
		this.main = main;
	}
	
}
