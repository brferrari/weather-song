package com.ifood.weathersong.spotify.client;

import java.nio.charset.Charset;
import java.util.Base64;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.ifood.weathersong.domain.MusicalGenre;
import com.ifood.weathersong.domain.Playlist;
import com.ifood.weathersong.domain.Track;
import com.ifood.weathersong.spotify.client.response.OAuthResponse;
import com.ifood.weathersong.spotify.client.response.SpotifyCategoryPlaylistResponse;
import com.ifood.weathersong.spotify.client.response.SpotifyPlaylistItemResponse;
import com.ifood.weathersong.spotify.client.response.SpotifyTrackResponse;

@Service
public class SpotifyClientService {
	
	private static final String URL = "https://api.spotify.com/v1";
	private static final String URI_PLAYLISTS = "/browse/categories/%s/playlists";
	private static final String URI_AUTHORIZE = "/authorize";
	
	@Value("${spotify.username}")
	private String userName;
	
	@Value("${spotify.password}")
	private String password;
	
	@Autowired
	private RestTemplate restTemplate;
	
	@Retryable(value = {RuntimeException.class}, maxAttempts = 3, backoff = @Backoff(delay = 300))
	public Playlist getPlaylist(final MusicalGenre musicalGenre) {
		
		final String finalUrl = String.format(URL + URI_PLAYLISTS, musicalGenre.getCategory());
		
		try {
			final HttpEntity<Object> requestEntity = new HttpEntity<>(this.buildHeader());
			final ResponseEntity<SpotifyCategoryPlaylistResponse> playlistResponse = 
			restTemplate.exchange(finalUrl, HttpMethod.GET, requestEntity, SpotifyCategoryPlaylistResponse.class);
			
			if (playlistResponse == null || !playlistResponse.hasBody()) {
				throw new RuntimeException(String.format("Não foi possível obter a playlist para o gênero [%s].", musicalGenre));
			}
			
			final Optional<SpotifyPlaylistItemResponse> playlistItem = playlistResponse.getBody().getPlaylists().getItems().stream().findFirst();
			
			if (!playlistItem.isPresent()) {
				throw new RuntimeException(String.format("Não foi possível obter os dados da playlist para o gênero [%s].", musicalGenre));
			}
			
			final String tracksUrl = playlistItem.get().getTracks().getHref();
			
			final ResponseEntity<SpotifyTrackResponse> tracksResponse = 
			restTemplate.exchange(tracksUrl, HttpMethod.GET, requestEntity, SpotifyTrackResponse.class);
			
			if (tracksResponse == null || !tracksResponse.hasBody()) {
				throw new RuntimeException(String.format("Não foi possível obter as faixas para o gênero [%s].", musicalGenre));
			}
			
			List<Track> tracks = tracksResponse.getBody().getItems()
					.stream().map(i -> new Track(i.getTrack().getName())).collect(Collectors.toList());
			
			final Playlist playlist = new Playlist();
			playlist.setTracks(tracks);
			
			return playlist;
			
		} catch (Exception e) {
			throw new RuntimeException("Não foi possível obter a playlist para o gênero informado.", e);
		}
		
	}
	
	private String getOAuthToken() {
		
		final String finalUrl = URL + URI_AUTHORIZE;
		
		final HttpEntity<Object> requestPlaylistEntity = new HttpEntity<>(this.buildBasicAuthenticationHeader());
		final ResponseEntity<OAuthResponse> response = 
		restTemplate.exchange(finalUrl, HttpMethod.GET, requestPlaylistEntity, OAuthResponse.class);
		
		if (response.hasBody()) {
			return response.getBody().getAccessToken();
		} else {
			return null;
		}
		
	}
	
    private HttpHeaders buildHeader() {
		
		final String oauthToken = getOAuthToken();
		
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", oauthToken);
		headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
		
		return headers;
		
	}
	
	private HttpHeaders buildBasicAuthenticationHeader() {
		
		HttpHeaders headers = new HttpHeaders();
		
		final String basicAuth = this.userName + ":" + this.password;
        
		final String encodedBasicAuth = "Basic " + Base64.getEncoder().encodeToString(basicAuth.getBytes(Charset.forName("UTF-8")));
				
        headers.set("Authentication", encodedBasicAuth);
        return headers;
		
	}

}
